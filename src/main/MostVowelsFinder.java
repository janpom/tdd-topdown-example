import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class MostVowelsFinder
{
	private static Set<Character> LC_VOWELS = new HashSet<Character>(Arrays.asList('a', 'e', 'i', 'o', 'u', 'y'));

	public static String findWordWithMostVowels(String commaSeparatedWords)
	{
		String wordWithMostVowels = null;
		int highestVowelCount = -1;

		for (String word : commaSeparatedWords.split(","))
		{
			int vowelCount = countVowels(word);

			if (vowelCount > highestVowelCount)
			{
				wordWithMostVowels = word;
				highestVowelCount = vowelCount;
			}
		}

		return wordWithMostVowels;
	}

	static int countVowels(String word)
	{
		int vowelCount = 0;

		for (char ch : word.toCharArray())
		{
			if (isVowel(ch))
				vowelCount++;
		}

		return vowelCount;
	}

	public static boolean isVowel(char ch)
	{
		return LC_VOWELS.contains(Character.toLowerCase(ch));
	}
}

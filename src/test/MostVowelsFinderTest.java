import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class MostVowelsFinderTest
{
	@Test
	public void testFindWordWithMostVowels() throws Exception
	{
		assertThat(MostVowelsFinder.findWordWithMostVowels("foo,bar,queue,pool"), equalTo("queue"));
	}

	@Test
	public void testCountVowels() throws Exception
	{
		assertThat(MostVowelsFinder.countVowels("prst"), is(0));
		assertThat(MostVowelsFinder.countVowels("foo"), is(2));
		assertThat(MostVowelsFinder.countVowels("couple"), is(3));
		assertThat(MostVowelsFinder.countVowels("queue"), is(4));
	}

	@Test
	public void testIsVowel() throws Exception
	{
		assertThat(MostVowelsFinder.isVowel('a'), is(true));
		assertThat(MostVowelsFinder.isVowel('A'), is(true));
		assertThat(MostVowelsFinder.isVowel('e'), is(true));
		assertThat(MostVowelsFinder.isVowel('O'), is(true));
		assertThat(MostVowelsFinder.isVowel('b'), is(false));
		assertThat(MostVowelsFinder.isVowel('F'), is(false));
		assertThat(MostVowelsFinder.isVowel('*'), is(false));
	}
}
